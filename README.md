This is a new project that is intended to work as a cheby extension to do two things:

1. Automatically create a wrapper for every IP that cheby creates

2. Make a configuration that will convert Wishbone interface to AXI automatically
