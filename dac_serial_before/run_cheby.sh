#!/bin/sh

NAME="dac_serial_regs"

# commands must be executed from script directory, where both script and .cheby files are
cd "$(dirname "$(readlink -f "$0")")"

mkdir -p doc
mkdir -p python

cheby --print-simple --gen-hdl=rtl/$NAME.vhd --gen-doc=doc/$NAME.html\
  --consts-style=python --gen-consts=python/${NAME}_const.py\
  -i $NAME.cheby

cheby --consts-style=vhdl --gen-consts=rtl/${NAME}_const_pkg.vhd -i $NAME.cheby
