#!/usr/bin/env python3
"""
Script used to invoke VUnit test framework.

To run (with GUI) type (preferably) from root of repository:
python3 path/to/this/file.py  -g
"""

import os
import sys
import random
from vunit import VUnit
from pathlib import Path
from itertools import product


def find_vcs_root(start, dirs=('.git',)):
    """Return absolute path to the root folder of current repository."""
    for dir in Path(start).resolve().parents:
        if any(dir.joinpath(d).is_dir() for d in dirs):
            return dir.resolve()
    raise Exception('no .git repository found')


def generate_tests(obj, prescalers, dwidths, dead_times, pre_pads, post_pads,
                   msb_firsts, fix_add_clk_cycs):
    """Generate tests for all possible generic combinations."""
    for pre, dw, dt, prepad, postpad, msb, fix_add_clk_cyc in product(
            prescalers, dwidths, dead_times, pre_pads, post_pads, msb_firsts,
            fix_add_clk_cycs):
        config_name = f"ps={pre}_dwidth={dw}_dtime={dt}_pre={prepad}"
        config_name += f"_post={postpad}_msb={msb}_addclk={fix_add_clk_cyc}"

        obj.add_config(
            name=config_name,
            generics=dict(dac_prescaler_g=pre, data_width_g=dw, dead_time_g=dt,
                          pre_pad_g=prepad, post_pad_g=postpad,
                          msb_first_g=msb, fix_add_clk_cyc_g=fix_add_clk_cyc)
            )


repo_root = find_vcs_root(Path(__file__))
sys.path.append(str(repo_root / "common/python"))
import sim_utils
# Add threading by default because we generate multiple tests
sys.argv.extend(['--num-threads', str(os.cpu_count())])
vu = VUnit.from_argv()
vu.add_osvvm()
vu.add_verification_components()

# create library and sources
uut = vu.add_library('uut')

uut.add_source_file(repo_root /
    "common/vhd/general-cores/wishbone/wishbone_pkg.vhd")
uut.add_source_file(repo_root /
    "common/vhd/interfaces/dac_serial/rtl/dac_serial.vhd")
uut.add_source_file(repo_root /
    "common/vhd/interfaces/dac_serial/rtl/dac_serial_regs.vhd")
uut.add_source_file(repo_root /
    "common/vhd/interfaces/dac_serial/rtl/dac_serial_regs_const_pkg.vhd")
uut.add_source_file(repo_root /
    "common/vhd/interfaces/dac_serial/rtl/wb_dac_serial.vhd")
# DAC model
uut.add_source_file(repo_root / "common/vhd/packages/tb/cce_tb_utils.vhd")
uut.add_source_file(repo_root /
    "common/vhd/interfaces/max5719_interface/src/rtl/max_5719.vhd")
# testbench
uut.add_source_file(repo_root /
    "common/vhd/interfaces/dac_serial/tb/tb_dac_serial.vhd")
uut.add_source_file(repo_root /
    "common/vhd/interfaces/dac_serial/tb/tb_wb_dac_serial.vhd")

for tb in uut.get_test_benches():
    tb.set_generic("rnd_seed_g", random.randrange(2**31))

for test in uut.test_bench("tb_dac_serial").get_tests():
    if test.name == "max5719_model":
        # specific configuration that work with our DAC model
        # 16 bit and 20 bit (with 4 bit padding) interface
        # minimum dead time depends on clock prescaler
        generate_tests(test, [1], [16], [30], [0], [0], [True], [False])
        generate_tests(test, [5], [16], [6], [0], [0], [True], [False])
        generate_tests(test, [1], [16], [30], [0], [0], [True], [True])
        generate_tests(test, [5], [16], [6], [0], [0], [True], [True])
        generate_tests(test, [1], [20], [30], [0], [4], [True], [False])
        generate_tests(test, [5], [20], [6], [0], [4], [True], [False])
        generate_tests(test, [1], [20], [30], [0], [4], [True], [True])
        generate_tests(test, [5], [20], [6], [0], [4], [True], [True])
    else:
        # basic clock, data width and endianess tests
        # don't repeat clock prescaler tests that were done with MAX model
        generate_tests(test, [2], [8, 16, 24], [1, 2, 3], [0], [0],
                       [False, True], [False])
        # pre and post padding tests
        generate_tests(test, [1, 2, 5], [20], [1, 2, 3], [0, 1, 4], [0, 1, 4],
                       [False, True], [False])

for test in uut.test_bench("tb_wb_dac_serial").get_tests():
    if test.name == "max5719_model":
        # specific configuration that work with our DAC model
        # 16 bit and 20 bit (with 4 bit padding) interface
        generate_tests(test, [1], [16], [30], [0], [0], [True], [False])
        generate_tests(test, [1], [16], [30], [0], [0], [True], [True])
        generate_tests(test, [1], [20], [30], [0], [4], [True], [False])
        generate_tests(test, [1], [20], [30], [0], [4], [True], [True])
    else:
        # basic clock, data width and endianess tests
        generate_tests(test, [1, 2, 5], [8, 16], [30], [0], [0], [False, True],
                       [False])
        # pre and post padding tests
        generate_tests(test, [1, 2, 5], [20], [30], [0, 1, 4], [0, 1, 4],
                       [False, True], [False])

sim_utils.vunit_enable_coverage(uut)

vu.main(post_run=sim_utils.vunit_postrun(Path(__file__).resolve()))
