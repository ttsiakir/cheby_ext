library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.vc_context;
library osvvm;
use osvvm.RandomPkg.all;

use work.dac_serial_pkg.all;
use work.dac_serial_regs_consts.all;
use work.wishbone_pkg.all;

entity tb_wb_dac_serial is
  generic(
    runner_cfg        : string;
    rnd_seed_g        : natural;
    dac_prescaler_g   : positive;
    data_width_g      : positive;
    dead_time_g       : positive;
    pre_pad_g         : natural;
    post_pad_g        : natural;
    msb_first_g       : boolean;
    fix_add_clk_cyc_g : boolean
  );
end entity;

architecture tb of tb_wb_dac_serial is

  constant CLK_PERIOD_C      : time    := 25 ns;
  constant DAC_DATA_RWIDTH_C : positive := pre_pad_g + data_width_g + post_pad_g;
  constant DOUT_PORTS_C      : natural := 16;

  -- Wishbone data and address width are determined by general-cores WB package.
  -- Historically, these were always 32 bits.
  constant c_wbm_handle : bus_master_t := new_bus(data_length => c_wishbone_data_width,
                                          address_length => c_wishbone_address_width);

  type slv_arr_t is array (natural range <>) of std_ulogic_vector;

  -- This variable is used by main process and tb procedures
  shared variable rnd : RandomPType;

  function to_sl(a : boolean) return std_logic is
  begin
    if a then
      return ('1');
    else
      return ('0');
    end if;
  end to_sl;

  procedure gen_samples(buf : out dac_data_arr) is
    variable tmp_int : integer;
  begin
    for i in buf'range loop
      tmp_int := rnd.RandInt(0, 2**data_width_g - 1);
      buf(i)  := std_ulogic_vector(to_unsigned(tmp_int, buf(i)'length));
    end loop;
  end procedure gen_samples;

  procedure compare_samples(got : in slv_arr_t; exp : in dac_data_arr) is
  begin
    check_equal(got'length, exp'length, "Array lengths must be equal for comparison");

    for i in got'range loop
      check_equal(got(i)(got(i)'high - pre_pad_g downto post_pad_g),
                  exp(i)(data_width_g - 1 downto 0),
                  "port(" & integer'image(i) & ") - SPI output differs from expected");
    end loop;
  end procedure compare_samples;

  procedure compare_max_output(got : in slv_arr_t; exp : dac_data_arr) is
  begin
    for i in got'range loop
      check_equal(got(i)(data_width_g - 1 downto 0), exp(i)(data_width_g - 1 downto 0),
                  "MAX analog output differs from expected");
    end loop;
  end procedure;

  signal tb_clk : std_ulogic := '0';
  signal tb_rst : std_ulogic;

  signal spi_mosi : std_ulogic_vector(DOUT_PORTS_C - 1 downto 0);
  signal spi_clk  : std_ulogic;
  signal spi_cs_n : std_ulogic;

  signal dac_ldac_n      : std_ulogic;
  signal true_dac_ldac_n : std_ulogic;

  signal max5719_dout       : slv_arr_t(DOUT_PORTS_C - 1 downto 0)(data_width_g - 1 downto 0);
  signal max5719_dout_latch : slv_arr_t(DOUT_PORTS_C - 1 downto 0)(data_width_g - 1 downto 0);
  signal max5719_dout_en    : std_ulogic_vector(DOUT_PORTS_C - 1 downto 0);

  signal spi_mosi_rcv : slv_arr_t(DOUT_PORTS_C - 1 downto 0)(DAC_DATA_RWIDTH_C - 1 downto 0);

  -- Wishbone interface
  signal wb_slave_i : t_wishbone_slave_in;
  signal wb_slave_o : t_wishbone_slave_out;

begin

  tb_clk <= not tb_clk after CLK_PERIOD_C/2;

  main : process
    variable tmp_int : integer;
    variable tmp_dat : dac_data_arr(DOUT_PORTS_C - 1 downto 0);
    variable wb_addr : natural;
    variable wb_data : std_ulogic_vector(c_wishbone_data_width-1 downto 0);
  begin
    test_runner_setup(runner, runner_cfg);
    rnd.InitSeed(rnd_seed_g);
    info("[STARTING] Simulation with seed = " & integer'image(rnd_seed_g));
    info("Starting tests");
    info("Resetting signals");
    wait for 1 ns;
    tb_rst <= '1';
    wait for 2 * CLK_PERIOD_C;
    tb_rst <= '0';
    wait for 2 * CLK_PERIOD_C;

    if run("max5719_model") then

      info("Configure DAC interface to MAX5719 parameters");
      wb_addr := ADDR_DAC_SERIAL_REGS_CONFIG;
      wb_data := (others => '0');
      wb_data(DAC_SERIAL_REGS_CONFIG_CPOL_OFFSET)      := '0';
      wb_data(DAC_SERIAL_REGS_CONFIG_CPHA_OFFSET)      := '0';
      wb_data(DAC_SERIAL_REGS_CONFIG_MSB_FIRST_OFFSET) := '1';
      wb_data(DAC_SERIAL_REGS_CONFIG_PRE_PAD_OFFSET + 2 downto
              DAC_SERIAL_REGS_CONFIG_PRE_PAD_OFFSET) := std_logic_vector(to_unsigned(pre_pad_g, 3));
      wb_data(DAC_SERIAL_REGS_CONFIG_POST_PAD_OFFSET + 2 downto
              DAC_SERIAL_REGS_CONFIG_POST_PAD_OFFSET) := std_logic_vector(to_unsigned(post_pad_g, 3));
      wb_data(DAC_SERIAL_REGS_CONFIG_DATA_WIDTH_OFFSET + 4 downto
              DAC_SERIAL_REGS_CONFIG_DATA_WIDTH_OFFSET) := std_logic_vector(to_unsigned(data_width_g, 5));
      wb_data(DAC_SERIAL_REGS_CONFIG_DEAD_TIME_OFFSET + 5 downto
              DAC_SERIAL_REGS_CONFIG_DEAD_TIME_OFFSET) := std_logic_vector(to_unsigned(dead_time_g, 6));
      wb_data(DAC_SERIAL_REGS_CONFIG_CLK_PRES_OFFSET + 5 downto
              DAC_SERIAL_REGS_CONFIG_CLK_PRES_OFFSET) := std_logic_vector(to_unsigned(dac_prescaler_g, 6));
      if fix_add_clk_cyc_g then
        wb_data(DAC_SERIAL_REGS_CONFIG_FIX_ADD_CLK_CYC_OFFSET + 3 downto
                DAC_SERIAL_REGS_CONFIG_FIX_ADD_CLK_CYC_OFFSET) := x"1";
      end if;
      write_bus(net, c_wbm_handle, wb_addr, wb_data);
      wait_until_idle(net, c_wbm_handle); -- workaround for VUnit bug (only happens with GHDL)
      for i in 0 to 4 loop -- just send a few samples
        wait until rising_edge(tb_clk);
        gen_samples(tmp_dat);

        -- first write data to be sent
        for k in tmp_dat'range loop
          wb_addr := ADDR_DAC_SERIAL_REGS_DATA + k * 4;
          wb_data := tmp_dat(k);
          write_bus(net, c_wbm_handle, wb_addr, wb_data);
          wait_until_idle(net, c_wbm_handle);
        end loop;
        -- then start the SPI transmission
        wb_addr := ADDR_DAC_SERIAL_REGS_CTRL;
        wb_data := (others => '0');
        wb_data(DAC_SERIAL_REGS_CTRL_START_OFFSET) := '1';
        write_bus(net, c_wbm_handle, wb_addr, wb_data);
        wait_until_idle(net, c_wbm_handle);
        -- wait until done
        wb_addr := ADDR_DAC_SERIAL_REGS_STATUS;
        wait_until_read_bit_equals(net, c_wbm_handle, std_logic_vector(to_unsigned(wb_addr, 32)),
                                   DAC_SERIAL_REGS_STATUS_BUSY_OFFSET, '0');

        compare_samples(spi_mosi_rcv, tmp_dat);
        compare_max_output(max5719_dout_latch, tmp_dat);
      end loop;
    end if;

    wait for 10 * CLK_PERIOD_C;
    test_runner_cleanup(runner); -- Simulation ends here
  end process;

  test_runner_watchdog(runner, 10 ms);

  dac_rcv : process(all)
  begin
    if falling_edge(spi_cs_n) then
      spi_mosi_rcv <= (others => (others => '0'));
    else
      if rising_edge(spi_clk) then
        if spi_cs_n = '0' then
          for i in spi_mosi_rcv'range loop
            spi_mosi_rcv(i) <= spi_mosi_rcv(i)(spi_mosi_rcv(i)'left - 1 downto 0) & spi_mosi(i);
          end loop;
        end if;
      end if;
    end if;
  end process;

  max_out_latch : process(all)
  begin
    for i in max5719_dout_en'range loop
      if max5719_dout_en(i) = '1' then
        max5719_dout_latch(i) <= max5719_dout(i);
      end if;
    end loop;
  end process;

  uut : entity work.wb_dac_serial
  generic map (
    active_ports_g => DOUT_PORTS_C
  )
  port map (
    wb_rst_i => tb_rst,
    wb_clk_i => tb_clk,
    wb_i     => wb_slave_i,
    wb_o     => wb_slave_o,

    dac_data_o => spi_mosi,
    dac_clk_o  => spi_clk,
    dac_cs_n_o => spi_cs_n,

    dac_ldac_n_o => true_dac_ldac_n
  );

  -- Wishbone master from VUnit verification library
  x_vunit_wb : entity vunit_lib.wishbone_master
  generic map (
    bus_handle              => c_wbm_handle,
    strobe_high_probability => 0.8
  )
  port map (
    clk   => tb_clk,
    adr   => wb_slave_i.adr,
    dat_i => wb_slave_o.dat,
    dat_o => wb_slave_i.dat,
    sel   => wb_slave_i.sel,
    cyc   => wb_slave_i.cyc,
    stb   => wb_slave_i.stb,
    we    => wb_slave_i.we,
    stall => wb_slave_o.stall,
    ack   => wb_slave_o.ack
  );

  models : for i in 0 to DOUT_PORTS_C - 1 generate
    -- MAX5719 model for external reference
    x_max_5719 : entity work.max_5719
    generic map (
      debug_level_g     => 0,
      n_bits_g          => data_width_g, --only 16 or 20 (+4 post-pad)
      use_err_g         => false,
      use_keyword_now_g => true -- enable timing checks
    )
    port map (
      sim_halt => false,

      cs_n_i   => spi_cs_n,
      ldac_n_i => dac_ldac_n,
      sck_i    => spi_clk,
      sdi_i    => spi_mosi(i),

      dac_o    => max5719_dout(i),
      dac_en_o => max5719_dout_en(i)
    );
  end generate;

  -- This imitates a "clever" circuit found on some boards like VS_DAC
  gen_ldac: if fix_add_clk_cyc_g generate
    dac_ldac_n <= spi_clk nand spi_cs_n;
  else generate
    dac_ldac_n <= true_dac_ldac_n;
  end generate gen_ldac;

end architecture;
