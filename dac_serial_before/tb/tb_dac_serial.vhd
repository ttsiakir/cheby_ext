library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;
library osvvm;
use osvvm.RandomPkg.all;

use work.dac_serial_pkg.all;

entity tb_dac_serial is
  generic(
    runner_cfg        : string;
    rnd_seed_g        : natural;
    dac_prescaler_g   : positive;
    data_width_g      : positive;
    dead_time_g       : positive;
    pre_pad_g         : natural;
    post_pad_g        : natural;
    msb_first_g       : boolean;
    fix_add_clk_cyc_g : boolean
  );
end entity;

architecture tb of tb_dac_serial is

  constant CLK_PERIOD_C      : time    := 25 ns;
  constant DAC_DATA_RWIDTH_C : positive := pre_pad_g + data_width_g + post_pad_g;
  constant DOUT_PORTS_C      : natural := 1;

  type slv_arr_t is array (natural range <>) of std_ulogic_vector;

  shared variable rnd : RandomPType;

  function to_sl(a : boolean) return std_logic is
  begin
    if a then
      return ('1');
    else
      return ('0');
    end if;
  end to_sl;

  function reverse(a : in std_ulogic_vector)
  return std_ulogic_vector is
    variable result : std_ulogic_vector(a'range);
  begin
    for i in a'range loop
      result(i) := a(a'high - i);
    end loop;
    return result;
  end function reverse;

  procedure gen_samples(buf : out dac_data_arr) is
    variable tmp_int : integer;
  begin
    for i in buf'range loop
      tmp_int := rnd.RandInt(0, 2**data_width_g - 1);
      buf(i)  := std_ulogic_vector(to_unsigned(tmp_int, buf(i)'length));
    end loop;
  end procedure gen_samples;

  procedure compare_samples(got : in slv_arr_t; exp : in dac_data_arr) is
    -- received data, but always in MSB form
    variable got_msb : std_ulogic_vector(data_width_g - 1 downto 0);
  begin
    check_equal(got'length, exp'length, "Array lengths must be equal for comparison");

    for i in got'range loop
      got_msb := got(i)(got(i)'high - pre_pad_g downto post_pad_g);
      if not msb_first_g then
        got_msb := reverse(got_msb);
      end if;

      check_equal(got_msb, exp(i)(data_width_g - 1 downto 0),
                  "port(" & integer'image(i) & ") - SPI output differs from expected");
      if pre_pad_g > 0 then
        check_equal(got(i)(got(i)'high downto got(i)'high - pre_pad_g + 1), 0,
                    "Pre-padding part should be all 0s");
      end if;
    end loop;
  end procedure compare_samples;

  procedure compare_max_output(got : in std_ulogic_vector; exp : std_ulogic_vector) is
  begin
    check_equal(got, exp(data_width_g - 1 downto 0), "MAX analog output differs from expected");
  end procedure;

  constant dac_data_len  : unsigned(4 downto 0) := to_unsigned(data_width_g,5);
  constant dac_pre_pad   : unsigned(2 downto 0) := to_unsigned(pre_pad_g, 3);
  constant dac_post_pad  : unsigned(2 downto 0) := to_unsigned(post_pad_g, 3);
  constant dac_dead_time : unsigned(5 downto 0) := to_unsigned(dead_time_g, 6);
  constant dac_prescaler : unsigned(5 downto 0) := to_unsigned(dac_prescaler_g, 6);

  signal tb_clk : std_ulogic := '0';
  signal tb_rst : std_ulogic;

  signal dac_data  : dac_data_arr(DOUT_PORTS_C - 1 downto 0);
  signal dac_start : std_ulogic;
  signal dac_busy  : std_ulogic;
  signal dac_done  : std_ulogic;
  signal spi_mosi  : std_ulogic_vector(DOUT_PORTS_C - 1 downto 0);
  signal spi_clk   : std_ulogic;
  signal spi_cs_n  : std_ulogic;
  signal max_cs_n  : std_ulogic;
  signal max_en    : std_ulogic;
  signal cpol      : std_ulogic;
  signal cpha      : std_ulogic;

  signal dac_ldac_n      : std_ulogic;
  signal true_dac_ldac_n : std_ulogic;
  signal max5719_dout    : std_ulogic_vector(data_width_g - 1 downto 0);
  signal max5719_dout_en : std_ulogic;

  signal spi_mosi_rcv : slv_arr_t(DOUT_PORTS_C - 1 downto 0)(DAC_DATA_RWIDTH_C - 1 downto 0);

begin

  tb_clk <= not tb_clk after CLK_PERIOD_C/2;

  main : process
    variable tmp_int : integer;
    variable tmp_dat : dac_data_arr(DOUT_PORTS_C - 1 downto 0);

    procedure send_samples is
    begin
      wait until rising_edge(tb_clk);
      gen_samples(tmp_dat);

      dac_data  <= tmp_dat;
      dac_start <= '1';
      wait until rising_edge(tb_clk);
      dac_start <= '0';
      dac_data  <= (others => (others => '0'));
        -- this can not be done in separate process as different
        -- numbers of pre_length and post_length will affect assert
      for i in 0 to DAC_DATA_RWIDTH_C - 1 loop
        if (cpol xor cpha) = '0' then
          wait until rising_edge(spi_clk);
        else
          wait until falling_edge(spi_clk);
        end if;
      end loop;
      wait for 1 ps; -- for delta cycle updates to propagate through
    end procedure send_samples;

  begin
    test_runner_setup(runner, runner_cfg);
    rnd.InitSeed(rnd_seed_g);
    info("[STARTING] Simulation with seed = " & integer'image(rnd_seed_g));
    info("Starting tests");
    info("Resetting signals");
    wait for 1 ns;
    tb_rst    <= '1';
    dac_start <= '0';
    dac_data  <= (others => (others => '0'));
    wait for 2 * CLK_PERIOD_C;
    tb_rst <= '0';
    wait for 2 * CLK_PERIOD_C;

    if run("max5719_model") then
      cpol   <= '0';
      cpha   <= '0';
      max_en <= '1';

      -- Watch out! This is not a standard loop, we iterate over SPI modes, so
      -- UUT waveforms are a bit diffent in each iteration
      for i in 0 to 4 loop -- just send a few samples
        send_samples;
        compare_samples(spi_mosi_rcv, tmp_dat);

        wait until rising_edge(spi_cs_n);
        -- The LDAC assertion should happen after the MAX5719 digital latency period
        -- signaled by `dout_en` port of behavioral model, otherwise MAX DAC will
        -- output previously sent (N-1) value instead of latest one
        wait until falling_edge(dac_ldac_n);
        wait until max5719_dout_en = '1';

        compare_max_output(max5719_dout, tmp_dat(0));
        if dac_busy = '1' then
          wait until dac_busy = '0';
        end if;
      end loop;

    elsif run("generic") then
      -- disable MAX5719 model because it doesn't support some configurations
      -- we test here
      max_en <= '0';

      for i in 0 to 3 loop
        (cpol, cpha) <= std_logic_vector(to_unsigned(i, 2));
        send_samples;
        compare_samples(spi_mosi_rcv, tmp_dat);
        if dac_busy = '1' then
          wait until dac_busy = '0';
        end if;
      end loop;

    end if;

    wait for 10 * CLK_PERIOD_C;
    test_runner_cleanup(runner); -- Simulation ends here
  end process;

  test_runner_watchdog(runner, 10 ms);

  dac_rcv : process(all)
  begin
    if falling_edge(spi_cs_n) then
      spi_mosi_rcv <= (others => (others => '0'));
    else
      -- In CPOL,CPHA=[(0,0), (1,1)] modes slave latches on rising edge, and in
      -- CPOL,CPHA=[(1,0), (0,1)] modes slave latches on falling edge
      if ((cpol xor cpha) = '0' and rising_edge(spi_clk))
         or ((cpol xor cpha) = '1' and falling_edge(spi_clk)) then
        for i in spi_mosi_rcv'range loop
          spi_mosi_rcv(i) <= spi_mosi_rcv(i)(spi_mosi_rcv(i)'left - 1 downto 0) & spi_mosi(i);
        end loop;
      end if;
    end if;
  end process;

  uut : entity work.dac_serial
  generic map(
    fix_add_clk_cyc_g => fix_add_clk_cyc_g
  )
  port map(
    clk_i     => tb_clk,
    rst_syn_i => tb_rst,

    data_i  => dac_data,
    start_i => dac_start,
    busy_o  => dac_busy,
    done_o  => dac_done,

    cpol_i            => cpol,
    cpha_i            => cpha,
    msb_first_i       => to_sl(msb_first_g),
    pre_pad_i         => std_logic_vector(dac_pre_pad),
    post_pad_i        => std_logic_vector(dac_post_pad),
    data_len_i        => std_logic_vector(dac_data_len),
    dead_time_i       => std_logic_vector(dac_dead_time),
    clk_div_i         => std_logic_vector(dac_prescaler),
    fix_add_clk_cyc_i => x"1",

    dac_data_o => spi_mosi,
    dac_clk_o  => spi_clk,
    dac_cs_n_o => spi_cs_n,

    dac_ldac_n_o => true_dac_ldac_n
  );

  -- MAX5719 model for external reference
  max_model_gen: if data_width_g = 16 or data_width_g = 20 generate
  begin
    x_max_5719 : entity work.max_5719
    generic map (
      debug_level_g     => 0,
      n_bits_g          => data_width_g, --only 16 or 20 (+4 post-pad)
      use_err_g         => false,
      use_keyword_now_g => true -- enable timing checks
    )
    port map (
      sim_halt => false,

      cs_n_i   => max_cs_n,
      ldac_n_i => dac_ldac_n,
      sck_i    => spi_clk,
      sdi_i    => spi_mosi(0),

      dac_o    => max5719_dout,
      dac_en_o => max5719_dout_en
    );

    max_cs_n <= spi_cs_n when max_en else '1';
  end generate max_model_gen;

  -- This imitates a "clever" circuit found on some boards like VS_DAC
  gen_ldac: if fix_add_clk_cyc_g generate
    dac_ldac_n <= spi_clk nand spi_cs_n;
  else generate
    dac_ldac_n <= true_dac_ldac_n;
  end generate gen_ldac;

end architecture;
