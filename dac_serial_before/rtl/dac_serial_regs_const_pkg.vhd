package dac_serial_regs_Consts is
  constant DAC_SERIAL_REGS_SIZE : Natural := 128;
  constant ADDR_DAC_SERIAL_REGS_CTRL : Natural := 16#0#;
  constant DAC_SERIAL_REGS_CTRL_RESET_OFFSET : Natural := 0;
  constant DAC_SERIAL_REGS_CTRL_START_OFFSET : Natural := 1;
  constant ADDR_DAC_SERIAL_REGS_CONFIG : Natural := 16#4#;
  constant DAC_SERIAL_REGS_CONFIG_CPOL_OFFSET : Natural := 0;
  constant DAC_SERIAL_REGS_CONFIG_CPHA_OFFSET : Natural := 1;
  constant DAC_SERIAL_REGS_CONFIG_MSB_FIRST_OFFSET : Natural := 2;
  constant DAC_SERIAL_REGS_CONFIG_PRE_PAD_OFFSET : Natural := 3;
  constant DAC_SERIAL_REGS_CONFIG_POST_PAD_OFFSET : Natural := 6;
  constant DAC_SERIAL_REGS_CONFIG_DATA_WIDTH_OFFSET : Natural := 9;
  constant DAC_SERIAL_REGS_CONFIG_DEAD_TIME_OFFSET : Natural := 14;
  constant DAC_SERIAL_REGS_CONFIG_CLK_PRES_OFFSET : Natural := 20;
  constant DAC_SERIAL_REGS_CONFIG_FIX_ADD_CLK_CYC_OFFSET : Natural := 26;
  constant ADDR_DAC_SERIAL_REGS_STATUS : Natural := 16#8#;
  constant DAC_SERIAL_REGS_STATUS_API_VER_OFFSET : Natural := 0;
  constant DAC_SERIAL_REGS_STATUS_DOUT_PORTS_OFFSET : Natural := 4;
  constant DAC_SERIAL_REGS_STATUS_BUSY_OFFSET : Natural := 8;
  constant ADDR_DAC_SERIAL_REGS_DATA : Natural := 16#40#;
  constant DAC_SERIAL_REGS_DATA_SIZE : Natural := 64;
  constant ADDR_DAC_SERIAL_REGS_DATA_0 : Natural := 16#40#;
  constant DAC_SERIAL_REGS_DATA_0_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_0_VALUE : Natural := 16#40#;
  constant ADDR_DAC_SERIAL_REGS_DATA_1 : Natural := 16#44#;
  constant DAC_SERIAL_REGS_DATA_1_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_1_VALUE : Natural := 16#44#;
  constant ADDR_DAC_SERIAL_REGS_DATA_2 : Natural := 16#48#;
  constant DAC_SERIAL_REGS_DATA_2_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_2_VALUE : Natural := 16#48#;
  constant ADDR_DAC_SERIAL_REGS_DATA_3 : Natural := 16#4c#;
  constant DAC_SERIAL_REGS_DATA_3_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_3_VALUE : Natural := 16#4c#;
  constant ADDR_DAC_SERIAL_REGS_DATA_4 : Natural := 16#50#;
  constant DAC_SERIAL_REGS_DATA_4_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_4_VALUE : Natural := 16#50#;
  constant ADDR_DAC_SERIAL_REGS_DATA_5 : Natural := 16#54#;
  constant DAC_SERIAL_REGS_DATA_5_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_5_VALUE : Natural := 16#54#;
  constant ADDR_DAC_SERIAL_REGS_DATA_6 : Natural := 16#58#;
  constant DAC_SERIAL_REGS_DATA_6_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_6_VALUE : Natural := 16#58#;
  constant ADDR_DAC_SERIAL_REGS_DATA_7 : Natural := 16#5c#;
  constant DAC_SERIAL_REGS_DATA_7_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_7_VALUE : Natural := 16#5c#;
  constant ADDR_DAC_SERIAL_REGS_DATA_8 : Natural := 16#60#;
  constant DAC_SERIAL_REGS_DATA_8_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_8_VALUE : Natural := 16#60#;
  constant ADDR_DAC_SERIAL_REGS_DATA_9 : Natural := 16#64#;
  constant DAC_SERIAL_REGS_DATA_9_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_9_VALUE : Natural := 16#64#;
  constant ADDR_DAC_SERIAL_REGS_DATA_10 : Natural := 16#68#;
  constant DAC_SERIAL_REGS_DATA_10_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_10_VALUE : Natural := 16#68#;
  constant ADDR_DAC_SERIAL_REGS_DATA_11 : Natural := 16#6c#;
  constant DAC_SERIAL_REGS_DATA_11_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_11_VALUE : Natural := 16#6c#;
  constant ADDR_DAC_SERIAL_REGS_DATA_12 : Natural := 16#70#;
  constant DAC_SERIAL_REGS_DATA_12_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_12_VALUE : Natural := 16#70#;
  constant ADDR_DAC_SERIAL_REGS_DATA_13 : Natural := 16#74#;
  constant DAC_SERIAL_REGS_DATA_13_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_13_VALUE : Natural := 16#74#;
  constant ADDR_DAC_SERIAL_REGS_DATA_14 : Natural := 16#78#;
  constant DAC_SERIAL_REGS_DATA_14_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_14_VALUE : Natural := 16#78#;
  constant ADDR_DAC_SERIAL_REGS_DATA_15 : Natural := 16#7c#;
  constant DAC_SERIAL_REGS_DATA_15_SIZE : Natural := 4;
  constant ADDR_DAC_SERIAL_REGS_DATA_15_VALUE : Natural := 16#7c#;
end package dac_serial_regs_Consts;
