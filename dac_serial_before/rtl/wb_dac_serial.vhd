-------------------------------------------------------------------------------
--! @file wb_dac_serial.vhd
--! @author Adrian Byszuk
--! @copyright CERN TE-EPC-CCE
--! @date 05-05-2021
--! @brief Serial DAC driver with Wishbone interface
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.dac_serial_pkg.all;

--! @brief Entity for serial DAC controller with Wishbone interface
--! @details
--! Number of active ports is configured at compile time
--! @section regmap Register memory map
--! @htmlinclude [dac_serial_regs.html]
entity wb_dac_serial is
  generic (
    --! Active data out ports (for topologies with many DACs connected to single clock line)
    active_ports_g : positive range 1 to DAC_MAX_PORTS_C := 1
  );
  port (
    --! @name Wishbone interface
    wb_rst_i : in std_logic; --! Wishbone/general reset
    wb_clk_i : in std_logic; --! Wishbone/system clock
    wb_i     : in t_wishbone_slave_in; --! Wishbone input ports
    wb_o     : out t_wishbone_slave_out; --! Wishbone output ports

    --! @name SPI interface
    dac_data_o : out std_logic_vector(active_ports_g - 1 downto 0); --! SPI serial data output
    dac_clk_o  : out std_logic; --! SPI clock (freq = Fclk_i / 2 / clk_div)
    dac_cs_n_o : out std_logic; --! SPI chip-select (negative polarity)

    --! @name Additional DAC signals (application-specific)
    dac_ldac_n_o : out std_logic  --! LDAC signal in MAXIM DACs
  );
end entity wb_dac_serial;


--! RTL implementation of wb_dac_serial
architecture rtl of wb_dac_serial is

  signal dac_reset   : std_ulogic;
  signal ctrl_reset  : std_ulogic;
  signal ctrl_start  : std_ulogic;
  signal status_busy : std_ulogic;
  signal data        : dac_data_arr(DAC_MAX_PORTS_C - 1 downto 0);

  signal config_cpol        : std_ulogic;
  signal config_cpha        : std_ulogic;
  signal config_msb         : std_ulogic;
  signal config_pre_pad     : std_logic_vector(2 downto 0);
  signal config_post_pad    : std_logic_vector(2 downto 0);
  signal config_data_len    : std_logic_vector(4 downto 0);
  signal config_dead_time   : std_logic_vector(5 downto 0);
  signal config_clk_div     : std_logic_vector(5 downto 0);
  signal config_fix_add_clk : std_logic_vector(3 downto 0);

begin

  dac_reset <= ctrl_reset or wb_rst_i;

  --! DAC serial controller
  x_dac_ctrl : entity work.dac_serial
  generic map (
    active_ports_g    => active_ports_g,
    fix_add_clk_cyc_g => true
  )
  port map (
    clk_i     => wb_clk_i,
    rst_syn_i => dac_reset,

    data_i  => data(active_ports_g - 1 downto 0),
    start_i => ctrl_start,
    busy_o  => status_busy,
    done_o  => open,

    cpol_i            => config_cpol,
    cpha_i            => config_cpha,
    msb_first_i       => config_msb,
    pre_pad_i         => config_pre_pad,
    post_pad_i        => config_post_pad,
    data_len_i        => config_data_len,
    dead_time_i       => config_dead_time,
    clk_div_i         => config_clk_div,
    fix_add_clk_cyc_i => config_fix_add_clk,

    dac_data_o   => dac_data_o,
    dac_clk_o    => dac_clk_o,
    dac_cs_n_o   => dac_cs_n_o,
    dac_ldac_n_o => dac_ldac_n_o
  );

  --! Wishbone registers (autogenerated)
  x_regs : entity work.dac_serial_regs
  port map (
    rst_n_i => not wb_rst_i,
    clk_i   => wb_clk_i,
    wb_i    => wb_i,
    wb_o    => wb_o,
    -- Control register
    ctrl_reset_o => ctrl_reset,
    ctrl_start_o => ctrl_start,
    -- DAC interface configuration
    config_cpol_o            => config_cpol,
    config_cpha_o            => config_cpha,
    config_msb_first_o       => config_msb,
    config_pre_pad_o         => config_pre_pad,
    config_post_pad_o        => config_post_pad,
    config_data_width_o      => config_data_len,
    config_dead_time_o       => config_dead_time,
    config_clk_pres_o        => config_clk_div,
    config_fix_add_clk_cyc_o => config_fix_add_clk,
    -- Status register
    status_api_ver_i       => x"2",
    status_dout_ports_i    => std_logic_vector(to_unsigned(active_ports_g, 4)),
    status_busy_i          => status_busy,
    -- Data ports
    data_0_value_o  => data(0),
    data_1_value_o  => data(1),
    data_2_value_o  => data(2),
    data_3_value_o  => data(3),
    data_4_value_o  => data(4),
    data_5_value_o  => data(5),
    data_6_value_o  => data(6),
    data_7_value_o  => data(7),
    data_8_value_o  => data(8),
    data_9_value_o  => data(9),
    data_10_value_o => data(10),
    data_11_value_o => data(11),
    data_12_value_o => data(12),
    data_13_value_o => data(13),
    data_14_value_o => data(14),
    data_15_value_o => data(15)
  );

end architecture rtl;
