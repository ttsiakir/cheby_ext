-------------------------------------------------------------------------------
--! @file dac_serial.vhd
--! @author Pawel Wozny, Adrian Byszuk <adrian.byszuk@cern.ch>
--! @copyright CERN SY-EPC-CCE
--! @date 2019-08-01
--! @brief Configurable controller for simple DACs with SPI interface
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package dac_serial_pkg is

  --! Maximum number of DOUT ports for DACs connected in parallel to the same clock
  constant DAC_MAX_PORTS_C : positive := 16;
  --! Maximum data word width
  constant DAC_MAX_DWIDTH_C : positive := 32;
  --! Auxiliary data type to avoid having multiple IN ports in IP interface (VHDL-93 limitation)
  type dac_data_arr is array (natural range <>) of std_logic_vector(DAC_MAX_DWIDTH_C-1 downto 0);

end package dac_serial_pkg;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.dac_serial_pkg.all;

--! @brief Configurable serial DAC (SPI, microwire) controller
--! @details
--! Implements SPI protocol with possible additional workarounds for "creative" ideas present
--! in RegFGC3 platform. All non-standard workarounds and extensions start with "fix_" prefix.
entity dac_serial is
  generic (
    --! Active data out ports (for topologies with many DACs connected to single clock line)
    active_ports_g : positive range 1 to DAC_MAX_PORTS_C := 1;
    --! Add SPI clock cycles after the end of transmition. Actual number controlled by input port.
    fix_add_clk_cyc_g : boolean := false
  );
  port (
    --! @name Clock and resets
    clk_i     : in std_logic; --! Main clock
    rst_syn_i : in std_logic; --! Synchronous reset

    --! @name Internal data interface
    --! @{

    --! Data value(s) to be sent, filled in (data_len_i downto 0) order. latched on start
    data_i  : in dac_data_arr(active_ports_g - 1 downto 0);
    --! Send data to the DAC(s), triggers on high level
    start_i : in std_ulogic;
    --! Data is being sent to the DAC(s)
    busy_o  : out std_logic;
    --! Finished sending data (pulse)
    done_o  : out std_logic;
    --! @}

    --! @name Internal configuration interface
    --! @{

    --! Clock polarity: '0' - idle low, '1' - idle high
    cpol_i            : in std_logic;
    --! Clock phase: slave latches on leading edge (0) or trailing edge (1)
    cpha_i            : in std_logic;
    --! Bit order - most significant bit is sent as first or last one
    msb_first_i       : in std_logic;
    --! Number of dummy clock cycles BEFORE beginning transfer of data
    pre_pad_i         : in std_logic_vector(2 downto 0);
    --! Number of dummy clock cycles AFTER transfer of data
    post_pad_i        : in std_logic_vector(2 downto 0);
    --! Number of bits in transmitted packet, must be stable during transmission
    data_len_i        : in std_logic_vector(4 downto 0);
    --! Length of wait time between end of SPI transfer and start of other tasks
    --! like LDAC assertion of generation of additional CLK pulses (in SPI clock cycles)
    dead_time_i       : in std_logic_vector(5 downto 0);
    --! Prescaler for output clock (must be >= 1)
    clk_div_i         : in std_logic_vector(5 downto 0);
    --! Number of clock pulses to add after end of dead time
    fix_add_clk_cyc_i : in std_logic_vector(3 downto 0);
    --! @}

    --! @name SPI interface
    dac_data_o : out std_logic_vector(active_ports_g - 1 downto 0); --! SPI serial data output
    dac_clk_o  : out std_logic; --! SPI clock (freq = Fclk_i / 2 / clk_div_i)
    dac_cs_n_o : out std_logic; --! SPI chip-select (negative polarity)

    --! @name Additional DAC signals (application-specific)
    dac_ldac_n_o : out std_logic  --! LDAC signal in MAXIM DACs. Sent after dead time (digital latency period)
  );
end dac_serial;

--! Behavioral implementation of serial DAC controller
architecture arch of dac_serial is

  --! State of main FSM
  type if_state_t is (IDLE, SEND_DATA, DEAD_TIME, DONE);
  signal if_state : if_state_t;
  --! Auxiliary clock FSM
  type clk_state_t is (LEADING_EDGE, TRAILING_EDGE);
  signal clk_state : clk_state_t;

  --! Internal clock divider
  signal clk_div_cnt : unsigned(clk_div_i'range);
  signal clk_strobe  : std_ulogic;
  signal clk_out_en  : std_ulogic;
  -- signals used to clock output data
  signal dac_data : dac_data_arr(active_ports_g - 1 downto 0);
  signal dac_clk  : std_ulogic;
  signal dac_cs   : std_ulogic;
  --! Dead time counter between rising CS_N and post-processing tasks, counts twice faster than SPI clock
  signal dead_time_cnt : unsigned(dead_time_i'high + 1 downto 0);
  --! Transmitted bytes counter
  signal data_cnt : natural range 0 to DAC_MAX_DWIDTH_C - 1;
  --! First bit transmission indicator
  signal first_bit : std_ulogic;
  --! Indicates that the serial data transmit part has finished (but not the whole FSM)
  signal transmit_done : std_ulogic;
  --! Pre-padding counter
  signal pre_pad_cnt : unsigned(pre_pad_i'range);
  --! Delayed signal to signal pre_pad phase to the output register block
  signal pre_pad_stage : std_ulogic;

begin

  --! Internal clock divider (based on loadable downcounter)
  p_clk_div : process(clk_i)
  begin
    if rising_edge(clk_i) then
      clk_strobe <= '0';
      if clk_out_en = '1' then
        clk_div_cnt <= clk_div_cnt - 1;
        -- The preloaded value will be N+1 wrt. to what we need. We can either do
        -- (clk_div_i - 1) while loading (but it's costly) or do (<= 1) comparison below
        -- <= or >= comparisons can be costly in case of bad synthesizer, but in <= 1 case
        -- we can just omit LSB in equality comparison
        if clk_div_cnt(clk_div_cnt'high downto 1) = 0 then
          clk_div_cnt <= unsigned(clk_div_i);
          clk_strobe  <= '1';
        end if;
      else -- clk_out_en = '0'
        -- reset to 0 so it immidiately loads divider in the first cycle when enabled
        clk_div_cnt <= (others => '0');
      end if;
    end if;
  end process;

  --! Main FSM process
  p_main : process(clk_i)
    procedure reset is
    begin
      dac_clk       <= cpol_i;
      dac_cs        <= '0';
      busy_o        <= '0';
      done_o        <= '0';
      dac_ldac_n_o  <= '1';
      clk_out_en    <= '0';
      transmit_done <= '0';
      pre_pad_stage <= '0';
      first_bit     <= '1';
      if_state      <= IDLE;
    end procedure;

  begin
    if rising_edge(clk_i) then
      case if_state is

        when IDLE =>
          reset;

          if start_i = '1' then
            busy_o      <= '1';
            dac_cs      <= '1';
            clk_out_en  <= '1';
            clk_state   <= LEADING_EDGE;
            pre_pad_cnt <= unsigned(pre_pad_i);
            -- post padding can be treated as extension of "real" data
            data_cnt <= to_integer(unsigned(data_len_i) + unsigned(post_pad_i));
            -- internal counter counts twice faster that external interface describes
            dead_time_cnt <= unsigned(dead_time_i) & '0';
            if_state      <= SEND_DATA;

            if unsigned(pre_pad_i) > 0 then
              pre_pad_stage <= '1';
            end if;
            if msb_first_i = '1' then
              for i in dac_data'range loop
                -- Data placement in shift register in MSB mode:
                -- (END)|0 0 (...) MSB MSB-1 MSB-2 (...) LSB+1 LSB|(START)
                dac_data(i) <= data_i(i);
              end loop;
            else -- reverse bits for LSB case
              -- The data length can be dynamically selected. This suggests that we should
              -- here not only invert bits, but also change the target index for each bit,
              -- depending on the 'data_len_i' value. But that would create a HUGE bank of
              -- 32 bit muxes for each target bit, and for each shift register!
              -- So let's keep a simple, static reverse assignment here.
              -- This way the LSB will land right at the shiftreg output, no muxing required!
              for i in dac_data'range loop
                for k in 0 to DAC_MAX_DWIDTH_C - 1 loop
                  -- (END)|LSB LSB+1 LSB+2 (...) MSB-1 MSB (...) 0 0|(START)
                  dac_data(i)(k) <= data_i(i)(DAC_MAX_DWIDTH_C - 1 - k);
                end loop;
              end loop;
            end if;

          end if; -- start_i = '1'

        when SEND_DATA =>
          if clk_strobe = '1' then

            case clk_state is
              when LEADING_EDGE =>
                dac_clk   <= not cpol_i;
                clk_state <= TRAILING_EDGE;
                if pre_pad_cnt = 0 then
                  first_bit     <= '0';
                  pre_pad_stage <= '0';
                end if;
                -- For CPHA=1 data was preloaded in IDLE state, so we must skip
                -- shifting register on the first bit, otherwise we'll lose it
                if cpha_i = '1' and first_bit = '0' then
                  -- we can reuse this also for post-padding because shiftreg is filled with '0's
                  data_cnt <= data_cnt - 1;
                  for i in dac_data'range loop
                    dac_data(i) <= dac_data(i)(dac_data(i)'high - 1 downto 0) & '0';
                  end loop;
                end if;

              when TRAILING_EDGE =>
                dac_clk   <= cpol_i;
                clk_state <= LEADING_EDGE;
                if pre_pad_cnt > 0 then
                  pre_pad_cnt <= pre_pad_cnt - 1;
                end if;
                -- transmission ends always on trailing edge
                if data_cnt = 1 then
                  data_cnt <= data_cnt;
                  if_state <= DEAD_TIME;
                elsif cpha_i = '0' and pre_pad_cnt = 0 then
                  -- in CPHA=0 mode data changes on trailing edge
                  -- don't shift out data in pre-padding phase
                  data_cnt <= data_cnt - 1;
                  for i in dac_data'range loop
                    dac_data(i) <= dac_data(i)(dac_data(i)'high - 1 downto 0) & '0';
                  end loop;
                end if;

            end case;
          end if;

        when DEAD_TIME =>
          if clk_strobe = '1' then
            dac_cs        <= '0';
            dead_time_cnt <= dead_time_cnt - 1;
            -- After dead time has passed, either go to DONE state or generate
            -- a few more clock cycles
            if dead_time_cnt(dead_time_cnt'high downto 1) = 0 then
              dead_time_cnt <= dead_time_cnt;
              transmit_done <= '1';
              -- We can add more clock cycles by just going back to SEND_DATA stage
              -- The "dac_data" shiftreg will be empty by this point so it won't generate any
              -- transitions on data output line
              if fix_add_clk_cyc_g and or_reduce(fix_add_clk_cyc_i) = '1' and transmit_done = '0' then
                data_cnt <= to_integer(unsigned(fix_add_clk_cyc_i));
                if_state <= SEND_DATA;
              else
                if_state <= DONE;
              end if;
            end if;
          end if;

        when DONE =>
          -- dac_ldac output should be optimized out by the tools if unused
          -- Must be applied *after* deasserting CS_N and *after* dead time period
          -- Datasheet is unclear here, but testing with MAX5719 shows that DAC
          -- will output last sent value *only* if LDAC was asserted after end of "digital latency period"
          dac_ldac_n_o <= '0';
          if clk_strobe = '1' then
            if_state <= IDLE;
            busy_o   <= '0';
            done_o   <= '1';
          end if;

        when others =>
          reset;

      end case;

      if rst_syn_i = '1' then
        reset;
      end if;
    end if;
  end process;

  --! Register clock/data outputs
  process(clk_i)
    variable pos : natural range 0 to DAC_MAX_DWIDTH_C - 1;
  begin
    if rising_edge(clk_i) then
      dac_clk_o <= dac_clk;
      for i in dac_data_o'range loop
        if pre_pad_stage = '1' then
          dac_data_o(i) <= '0';
        elsif msb_first_i = '1' then
          pos := to_integer(unsigned(data_len_i) - 1);
          -- In MSB first mode data is placed at the beginning in shift register like this:
          --      <  <  <  <  < shift direction <  <  <  <  <
          -- (END)|0 0 (...) MSB MSB-1 MSB-2 (...) LSB+1 LSB|(START)
          --                  ^ - bit selected by 'data_len_i' port (with optional shift by pre_pad)
          dac_data_o(i) <= dac_data(i)(pos);
        else
          -- In LSB mode the lowest bit is already placed at the last stage of shift register:
          --      <  <  <  <  < shift direction <  <  <  <  <
          -- (END)|(pre_pad 0s) LSB LSB+1 LSB+2 (...) MSB-1 MSB (...) 0 0|(START)
          --                     ^ - bit selection can be static
          dac_data_o(i) <= dac_data(i)(dac_data(i)'high);
        end if;
      end loop;
    end if;
  end process;

  dac_cs_n_o <= not dac_cs;

  assert unsigned(clk_div_i) /= 0
  report "Clock prescaler can't be zero!" severity FAILURE;

end arch;
