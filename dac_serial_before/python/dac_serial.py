"""Convenience methods for serial DAC controller."""

import dac_serial_regs_const as const

MAX_DOUT_PORTS = 16


class dac_serial:
    """
    Provides access to all module functionality and registers.

    Also implements custom methods for most used functionality.
    """

    def __init__(self, bus, offset):
        """Class constructor.

        :param bus: Communication object providing read() and write() methods
        :param offset: IP base address in memory map
        """
        self._bus = bus
        self._offset = offset

    def read(self, addr):
        """Read one of module registers.

        :param addr: Register relative address
        :return: Data read from register
        """
        assert addr <= const.DAC_SERIAL_REGS_SIZE
        return self._bus.read(self._offset + addr)

    def write(self, addr, data):
        """Write to one of module registers.

        :param addr: Register relative address
        :param data: Register data to be written
        """
        assert addr <= const.DAC_SERIAL_REGS_SIZE
        self._bus.write(self._offset + addr, data)

    def set_config(self, cpol, cpha, msb_first, pre_pad, post_pad, data_width,
                   dead_time, clk_pres, fix_add_clk_cyc=0):
        """Set DAC interface configuration.

        :param cpol: Clock polarity (0 - clock idle low, 1 - clock idle high)
        :param cpha: Clock phase (0 - DAC latch on leading edge, 1 - trailing)
        :param msb_first: Transfer most significant bit as first or last one
        :param pre_pad: Number of '0' bits before data
        :param post_pad: Number of '0' bits after data
        :param data_width: Width (in bits) of data sent to DAC. Depends on
        target DAC chip
        :param dead_time: Dead time after SPI transfer and other DAC tasks
        :param clk_pres: SPI clock prescaler.
        Final SPI frequency is Fclk / 2 / clk_pres
        :param fix_add_clk_cyc: Number of SPI clock cycles to add after the
        transmission (fix required for some DACs and boards)
        """
        assert 0 <= pre_pad <= 7
        assert 0 <= post_pad <= 7
        assert 1 <= data_width <= 32
        assert 0 <= dead_time <= 63
        assert 1 <= clk_pres <= 63
        assert 0 <= fix_add_clk_cyc <= 15

        val = 0
        val |= (cpol << const.DAC_SERIAL_REGS_CONFIG_CPOL_OFFSET)
        val |= (cpha << const.DAC_SERIAL_REGS_CONFIG_CPHA_OFFSET)
        val |= (msb_first << const.DAC_SERIAL_REGS_CONFIG_MSB_FIRST_OFFSET)
        val |= (pre_pad << const.DAC_SERIAL_REGS_CONFIG_PRE_PAD_OFFSET)
        val |= (post_pad << const.DAC_SERIAL_REGS_CONFIG_POST_PAD_OFFSET)
        val |= (data_width << const.DAC_SERIAL_REGS_CONFIG_DATA_WIDTH_OFFSET)
        val |= (dead_time << const.DAC_SERIAL_REGS_CONFIG_DEAD_TIME_OFFSET)
        val |= (clk_pres << const.DAC_SERIAL_REGS_CONFIG_CLK_PRES_OFFSET)
        val |= (fix_add_clk_cyc <<
                const.DAC_SERIAL_REGS_CONFIG_FIX_ADD_CLK_CYC_OFFSET)
        self.write(const.ADDR_DAC_SERIAL_REGS_CONFIG, val)

    def reset(self):
        """Reset the DAC controller."""
        self.write(const.ADDR_DAC_SERIAL_REGS_CTRL,
                   const.DAC_SERIAL_REGS_CTRL_RESET)

    def start(self):
        """Start transmission to DAC, block until done."""
        self.write(const.ADDR_DAC_SERIAL_REGS_CTRL,
                   const.DAC_SERIAL_REGS_CTRL_START)
        while True:
            val = self.read(const.ADDR_DAC_SERIAL_REGS_STATUS)
            if (val & const.DAC_SERIAL_REGS_STATUS_BUSY) == 0:
                return

    def write_data(self, data):
        """Write data to be sent to DAC(s).

        :param data: List of data samples to be sent to corresponding DAC(s)
        """
        assert len(data) <= MAX_DOUT_PORTS
        for i, d in enumerate(data):
            self.write(const.ADDR_DAC_SERIAL_REGS_DATA + 4*i, d)

    def get_api_version(self):
        """Get the register API version."""
        val = self.read(const.ADDR_DAC_SERIAL_REGS_STATUS)
        val = ((val & const.DAC_SERIAL_REGS_STATUS_API_VER) >>
               const.DAC_SERIAL_REGS_STATUS_API_VER_OFFSET)
        return val

    def get_active_ports(self):
        """Get the number of active data output ports."""
        val = self.read(const.ADDR_DAC_SERIAL_REGS_STATUS)
        val = ((val & const.DAC_SERIAL_REGS_STATUS_DOUT_PORTS) >>
               const.DAC_SERIAL_REGS_STATUS_DOUT_PORTS_OFFSET)
        return val

    def get_clk_prescaler(self):
        """Get the clock prescaler for the output SPI clock."""
        val = self.read(const.ADDR_DAC_SERIAL_REGS_STATUS)
        val = ((val & const.DAC_SERIAL_REGS_STATUS_CLK_PRESCALER) >>
               const.DAC_SERIAL_REGS_STATUS_CLK_PRESCALER_OFFSET)
        return val

